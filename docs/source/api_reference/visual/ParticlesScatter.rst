ParticlesScatter
================
.. toctree::
    :maxdepth: 3

.. autoclass:: tfv.visual.ParticlesScatter
    :members: