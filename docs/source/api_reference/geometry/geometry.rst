old style
=========
.. toctree::
    :maxdepth: 3

.. autofunction:: tfv.geometry.is_ccw

.. autofunction:: tfv.geometry.is_intersection

.. autofunction:: tfv.geometry.get_intersection

.. autofunction:: tfv.geometry.get_unit_vectors

.. autoclass:: tfv.geometry.Mesh
    :members: