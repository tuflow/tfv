Functions
=========

.. toctree::
    :maxdepth: 2

    is_ccw
    is_intersection
    get_intersection
    get_unit_vectors
