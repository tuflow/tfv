# TFV
The tfv package is a suite of tools for post-processing results from the [TUFLOW FV](https://www.tuflow.com/Tuflow%20FV.aspx) hydrodynamic, sediment transport, water quality and particle tracking modelling software. 

It is also provides a basic framework for extracting and visualizing 2D and 3D oceanographic and atmospheric model result data on unstructured meshes.

## Installing
The tfv package is available via the Python Package Index ([PyPi](https://pypi.org/project/tfv/)) or [Conda Forge](https://github.com/conda-forge/tfv-feedstock).

*Note: The latest version has been built and tested on Python 3.9 to 3.12*.

To install tfv from the conda command line tool:

```
conda install -c conda-forge tfv
```

Alternatively to install tfv using pip:

```
python -m pip install tfv
```

## Dependencies
The tfv package depends on the following core packages:

```
matplotlib >= 3.2.2
netCDF4 >= 1.5.3
numpy >= 1.19.0
xarray>=v2022.03.0
dask>=2022.01.0
scipy>=1.6.0
tqdm>=4.50.0
```

These will be automatically installed or updated as part of the tfv installation.

The following packages provide additional functionality and are **not** installed by default:

`ipywidgets >= 8.0.0` - Required for interactive results vizulisation in an interactive session (e.g., JuypterLab)

`PyQt5 >= 5.15.0`  - Required for interactive popout results vizulisation

## Tutorials, Documentation & Support
See the [API Reference](https://tfv.readthedocs.io/en/latest/) for code documentation and
[TUFLOW FV Python Tools](https://fvwiki.tuflow.com/index.php?title=FV_Python_Tools) for tutorials and demonstration 
datasets. 

For support contact [TUFLOW Support](mailto:support@tuflow.com).

## License
This project is licensed under the MIT License - see the [LICENSE.txt](https://gitlab.com/TUFLOW/tfv/blob/master/LICENSE) file for details
